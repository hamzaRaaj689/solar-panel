$(document).ready(function() {
    $('.nav-button').click(function() {
        $('.nav-button').toggleClass('change');
    });

    $(window).scroll(function() {
        let position = $(this).scrollTop();
        if (position >= 200) {
            $('.nav-menu').addClass('custom-navbar');
        } else {
            $('.nav-menu').removeClass('custom-navbar');
        }
    });

    $(window).scroll(function() {
        let position = $(this).scrollTop();
        if (position >= 650) {
            $('.camera-img').addClass('fromLeft');
            $('.mission-text').addClass('fromRight');
        } else {
            $('.camera-img').removeClass('fromLeft');
            $('.mission-text').removeClass('fromRight');
        }
    });


    // $('.nav-button').click(function(){
    //     count++;
    //     if(count%2!=0){
    //         $('.banner-heading').empty();
    //     }
    // });

    $('gallery-list-item').click(function() {
        let value = $(this).attr('data-filter');
        if (value == 'all') {
            $('.filter').show(300);
        } else {
            $('.filter').not('.' + value).hide(300);
            $('.filter').not('.' + value).show(300);
        }
    });

    $('.gallery-list-item').click(function() {
        $(this).addClass('active-item').siblings().removeClass('active-item');
    });
    $(window).scroll(function() {
        let positon = $(this).scrollTop();
        if (positon >= 4300) {
            $('.card-1').addClass('moveFromLeft');
            $('.card-1').addClass('moveFromBottom');
            $('.card-1').addClass('moveFromRight');
        } else {
            $('.card-1').addClass('moveFromLeft');
            $('.card-1').addClass('moveFromBottom');
            $('.card-1').addClass('moveFromRight');
        }
    });

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 3,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    });

});

var count = 0;
var leadObject = { "campaignId": "0", "productId": "", "productName": "", "name": "", "phone": "", "address": "", "zipCode": "", "province": "", "city": "", "country": "", "email": "", "quantity": 1, "price": 0, "shipping": "200", "message": "", "isRead": "false", "id": "", "createdOn": "2019-06-28T17:46:49.162Z", "lastModifiedOn": "2019-06-28T17:46:49.162Z", "settings": null, "settingsJson": { "fullName": null, "email": null, "phone": null, "address": null, "message": null, "productId": null, "productName": null, "price": null } };
console.log(leadObject);

function submitLead() {
    console.log($("#name").val());
    console.log($("#email").val());
    console.log($("#phone").val());
    console.log($("#message").val());
    leadObject.settingsJson.fullName = $("#name").val();
    // leadObject.settings.name=$("#name").val();      
    leadObject.settingsJson.email = $("#email").val();
    leadObject.settingsJson.phone = $("#phone").val();
    leadObject.settingsJson.message = $("#message").val();
    $.ajax({
        type: "POST",
        url: "api/Customer",
        data: leadObject,
        success: function(data) {

        },
        dataType: "application/json"
    });

}